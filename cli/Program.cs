﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using core;
using StackExchange.Redis;
//using core;

namespace scores.console
{

    class Program
    {
        static Task[] RunTasks(params Task[] tasks)
        {
            Task.WaitAll(tasks);
            return tasks;
        }

        static Task<T>[] RunTasks<T>(params Task<T>[] tasks)
        {
            Task.WaitAll(tasks);
            return tasks;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("connecting to redis");
            using (var redis = ConnectionMultiplexer.Connect("localhost"))
            {
                var db = redis.GetDatabase();
                string scoresKey = "scores_set";
                string hashKey = "users_scores";

                var tasks = RunTasks(
                    db.KeyDeleteAsync(scoresKey),
                    db.KeyDeleteAsync(hashKey));

                var scoreBoard = new ScoreBoard(db, scoresKey);

                var increaseTasks = new Task<Tuple<long, long?>>[]{
                 scoreBoard.AddScoreAsync("user1", 100),
                 scoreBoard.AddScoreAsync("user2", 200),
                 scoreBoard.AddScoreAsync("user3", 500),
                //  scoreBoard.AddScoreAsync("user1", 100),
                //  scoreBoard.AddScoreAsync("user3", 200)
                };

                Task.WaitAll(increaseTasks);

            }
        }
    }
}
