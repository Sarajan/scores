FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app/scores-cli

# COPY scores/cli/*.csproj ./scores-cli
# RUN dotnet restore

COPY ./cli/publish/. ./

CMD [ "./scores-cli" ]