using System;
using System.Threading.Tasks;

namespace core
{
    public interface IScoreBoard
    {
        Task<Tuple<long, long?>> AddScoreAsync(string userName, long score);
    }
}