using System;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace core
{
    public class ScoreBoard : IScoreBoard
    {
        const string ScoreField = "score";
        private readonly IDatabase _database;
        private readonly string ScoresSortedSetKey;
        
        protected IDatabase Database { get => _database; }

        public ScoreBoard(
            IDatabase database,
            string scoreBoard
            )
        {
            _database = database;
            ScoresSortedSetKey = scoreBoard;
        }
        public async Task<Tuple<long, long?>> AddScoreAsync(string userName, long score)
        {
            var totalScore = await Database.HashIncrementAsync(userName, ScoreField, score);
            var result = await Database.SortedSetAddAsync(ScoresSortedSetKey, totalScore, totalScore);
            var rank = await Database.SortedSetRankAsync(ScoresSortedSetKey, totalScore, Order.Descending);
            return Tuple.Create(totalScore, rank);
        }

        // public async Task<long?> GetRank(string userName)
        // {
        //     return await Database.SortedSetRankAsync(SortedSetKey, userName, Order.Descending);
        // }

        // public async Task<long?> GetLowerRankedUsers(string userName)
        // {
        //     return await Database.SortedSet(_key, userName, Order.Descending);
        // }

        // public async Task<long?> GetHigherRankedUsers(string userName)
        // {
        //     return await Database.SortedSetRankAsync(_key, userName, Order.Descending);
        // }
    }
}